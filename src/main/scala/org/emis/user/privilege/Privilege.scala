package org.emis.user

/**
  * Created by vselivanova on 29.01.2016.
  */
package object Privilege {

  object AccessPrivilege {

    case object Create
    case object UpdateName
    case object UpdatePassword

    case object Delete
    case object Get
  }

}

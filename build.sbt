organization := "com.privilage.lib"

name := "privilege_library"

homepage := Some(url("https://bitbucket.org/ViktoriiaSelivanova/privilagelib"))

version := "1.0"

scalaVersion := "2.11.7"
